package aakash.com.readsmssample.injection.module

import android.content.Context
import dagger.Module
import dagger.Provides



/**
 * Created by stllpt031 on 12/2/19.
 */
@Module
open class MainActivityModule(private val context: Context) {
    @Provides
    fun context(): Context {
        return context
    }
}