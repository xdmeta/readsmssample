package aakash.com.readsmssample.injection.component

import aakash.com.readsmssample.injection.module.MainActivityModule
import aakash.com.readsmssample.ui.smslisting.view.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by stllpt031 on 12/2/19.
 */
@Singleton
@Component(modules = [MainActivityModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}