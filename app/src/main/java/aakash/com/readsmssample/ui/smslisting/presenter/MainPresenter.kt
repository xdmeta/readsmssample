package aakash.com.readsmssample.ui.smslisting.presenter

import aakash.com.readsmssample.extension.checkDifference
import aakash.com.readsmssample.extension.getDifferenceInHour
import aakash.com.readsmssample.ui.smslisting.model.SmsData
import android.content.Context
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import javax.inject.Inject

/**
 * Created by stllpt031 on 12/2/19.
 */
class MainPresenter @Inject constructor(private val context: Context) {

    private lateinit var mView: View
    private var page = 0
    private var perPage = 10

    private var firstCheck = false
    private var oneHourCheck = false
    private var twoHourCheck = false
    private var threeHourCheck = false
    private var sixHourCheck = false
    private var twelveHourCheck = false
    private var oneDayCheck = false
    private var c : Cursor ?= null

    fun injectView(mView: View) {
        this.mView = mView
    }

    fun fetchSmsList() {
        val smsList = ArrayList<SmsData>()

        val uri = Uri.parse("content://sms/inbox")
        val c = context.contentResolver.query(uri, null, null, null, "  date DESC limit $perPage offset $page")
        (mView as AppCompatActivity).startManagingCursor(c)

        c?.let {
            // Read the sms data and store it in the list
            if (c.moveToFirst()) {
                for (i in 0 until c.count) {
                    val body = (c.getString(c.getColumnIndexOrThrow("body")).toString())
                    val number = (c.getString(c.getColumnIndexOrThrow("address")).toString())
                    val date =
                        if (c.getColumnIndex("date") != -1) (c.getString(c.getColumnIndexOrThrow("date")).toString()) else ""
                    val dateSent =
                        if (c.getColumnIndex("date_sent") != -1) (c.getString(c.getColumnIndexOrThrow("date_sent")).toString()) else ""
                    val sms: SmsData? = try {
                        val dateTimeStamp = date.toLong()
                        when {
                            (!firstCheck && dateTimeStamp.getDifferenceInHour() <= 1) -> {
                                firstCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            (!oneHourCheck && dateTimeStamp.getDifferenceInHour() in 1..2) -> {
                                oneHourCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            (!twoHourCheck && dateTimeStamp.getDifferenceInHour() in 2..3) -> {
                                twoHourCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            (!threeHourCheck && dateTimeStamp.getDifferenceInHour() in 3..6) -> {
                                threeHourCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            (!sixHourCheck && dateTimeStamp.getDifferenceInHour() in 6..12) -> {
                                sixHourCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            (!twelveHourCheck && dateTimeStamp.getDifferenceInHour() in 12..24) -> {
                                twelveHourCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            (!oneDayCheck && dateTimeStamp.getDifferenceInHour() > 24) -> {
                                oneDayCheck = true
                                SmsData(null, dateTimeStamp.checkDifference(), "", date, dateSent)
                            }
                            else -> null
                        }
                    } catch (e: Exception) {
                        null
                    }
                    sms?.let { it1 -> smsList.add(it1) }
                    smsList.add(SmsData(number, body, "", date, dateSent))
                    c.moveToNext()
                }
            }
        }
        mView.onSMSListFetch(smsList)
        page++
    }

    fun dispose() {
        c?.close()
    }

    interface View {
        fun onSMSListFetch(itemList: ArrayList<SmsData>)
        fun onSMSListFetchFailed(message: String)
    }
}