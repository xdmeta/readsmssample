package aakash.com.readsmssample.ui.smslisting.view

import aakash.com.readsmssample.AppApplication
import aakash.com.readsmssample.R
import aakash.com.readsmssample.extension.showSnack
import aakash.com.readsmssample.ui.managesms.SmsReceiver
import aakash.com.readsmssample.ui.smslisting.model.SmsData
import aakash.com.readsmssample.ui.smslisting.presenter.MainPresenter
import android.Manifest
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sokarcreative.library.BasicStuffItemDecoration
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainPresenter.View {
    @Inject
    lateinit var presenter: MainPresenter
    private var messageList = ArrayList<SmsData?>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as AppApplication).mComponent.inject(this)
        presenter.injectView(this)
        initView()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(Manifest.permission.READ_SMS), 100)
        } else {
            presenter.fetchSmsList()
        }
        registerReceiver(SmsReceiver(), IntentFilter())
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            100 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.fetchSmsList()
                } else {
                    showError(getString(R.string.err_permission_denied))
                }
            }
            else -> {
            }
        }
    }

    private var isLoading: Boolean = false

    private fun initView() {
        rvContent.layoutManager = LinearLayoutManager(this)
        val adapter = MainAdapter(messageList)
        rvContent.addItemDecoration(BasicStuffItemDecoration(adapter))
        rvContent.adapter = adapter
        rvContent.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = rvContent.layoutManager?.childCount ?: 0
                val totalItemCount = rvContent.layoutManager?.itemCount ?: 0
                val firstVisibleItemPosition =
                    (rvContent.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                if (!isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        isLoading = true
                        messageList.add(null)
                        rvContent.adapter?.notifyItemInserted(messageList.size)
                        presenter.fetchSmsList()
                    }
                }

            }
        })
    }

    override fun onSMSListFetch(itemList: ArrayList<SmsData>) {
        if (itemList.isEmpty()) {
            isLoading = true
            showError(getString(R.string.err_empty_inbox))
        } else {
            isLoading = false
            messageList.remove(null)
            messageList.addAll(itemList)
            rvContent.adapter?.notifyDataSetChanged()
        }
    }

    override fun onSMSListFetchFailed(message: String) {
        showError(message)
    }

    private fun showError(message: String) {
        isLoading = true
        messageList.remove(null)
        rvContent.adapter?.notifyItemRemoved(messageList.size)
        tvError.text = message
        rlContent.showSnack(message)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
    }
}