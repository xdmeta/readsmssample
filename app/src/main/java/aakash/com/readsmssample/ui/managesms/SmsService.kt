package aakash.com.readsmssample.ui.managesms

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log


/**
 * Created by stllpt031 on 13/2/19.
 */
class SmsService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        Log.d("SmsService"," : intent : $intent")
        return null
    }

    private var mSMSreceiver: SmsReceiver? = null
    private var mIntentFilter: IntentFilter? = null

    override fun onCreate() {
        super.onCreate()

        //SMS event receiver
        mSMSreceiver = SmsReceiver()
        mIntentFilter = IntentFilter()
        mIntentFilter!!.addAction("android.provider.Telephony.SMS_RECEIVED")
        registerReceiver(mSMSreceiver, mIntentFilter)
    }

    override fun onDestroy() {
        super.onDestroy()

        // Unregister the SMS receiver
        unregisterReceiver(mSMSreceiver)
    }
}