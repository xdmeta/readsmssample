package aakash.com.readsmssample.ui.smslisting.view

import aakash.com.readsmssample.R
import aakash.com.readsmssample.extension.getDate
import aakash.com.readsmssample.ui.smslisting.model.SmsData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sokarcreative.library.BasicStuffAdapter
import kotlinx.android.synthetic.main.layout_message.view.*

/**
 * Created by stllpt031 on 12/2/19.
 */
class MainAdapter(private val messageList: ArrayList<SmsData?>) : BasicStuffAdapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_PROGRESS = 0
        private const val TYPE_CONTENT = 1
        private const val TYPE_HEADER = 2
    }

    override fun isHeader(viewType: Int) = (viewType == TYPE_HEADER)

    override fun isStickyHeader(viewType: Int) = (viewType == TYPE_HEADER)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_CONTENT) {
            holder.itemView.apply {
                tvNumber.text = messageList[position]?.phoneNumber
                tvMessage.text = "${messageList[position]?.body} " +
                        "\n\n${messageList[position]?.date?.toLong()?.getDate()}"
            }
        } else if (getItemViewType(position) == TYPE_HEADER) {
            (holder.itemView as TextView).text = messageList[position]?.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            TYPE_PROGRESS -> MainHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.layout_progress,
                    parent, false
                )
            )
            TYPE_HEADER -> HeaderHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_message_header, parent, false)
            )
            else -> ProgressHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_message, parent, false)
            )
        }

    override fun getItemCount() = messageList.size

    override fun getItemViewType(position: Int): Int {
        messageList[position]?.let { it ->
            it.phoneNumber?.let {
                return TYPE_CONTENT
            } ?: let {
                return TYPE_HEADER
            }
        } ?: let {
            return TYPE_PROGRESS
        }
    }

    inner class MainHolder(view: View) : RecyclerView.ViewHolder(view)
    inner class ProgressHolder(view: View) : RecyclerView.ViewHolder(view)
    inner class HeaderHolder(view: View) : RecyclerView.ViewHolder(view)
}