package aakash.com.readsmssample.ui.smslisting.model

/**
 * Created by stllpt031 on 12/2/19.
 */
data class SmsData(val phoneNumber: String?,
                   val body: String,
                   val person: String?,
                   val date : String?,
                   val dateSent : String?)