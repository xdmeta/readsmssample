package aakash.com.readsmssample.ui.managesms

import aakash.com.readsmssample.R
import aakash.com.readsmssample.ui.smslisting.view.MainActivity
import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import androidx.core.app.NotificationCompat


/**
 * Created by stllpt031 on 12/2/19.
 */
open class SmsReceiver : BroadcastReceiver() {
    private val pdu_type = "pdus"

    @TargetApi(Build.VERSION_CODES.M)
    @Suppress("UNCHECKED_CAST")
    override fun onReceive(context: Context?, intent: Intent?) {
        // Get the SMS message.
        println("context = [${context}], intent = [${intent}]")
        val bundle = intent?.extras
        val msgs: ArrayList<SmsMessage>
        var strMessage = ""
        val format = bundle?.getString("format")
        val pdus = bundle?.get(pdu_type) as Array<Any>?
        if (pdus != null) {
            // Check the Android version.
            val isVersionM = (Build.VERSION.SDK_INT >=
                    Build.VERSION_CODES.M)
            msgs = ArrayList(pdus.size)
            for (i in 0 until msgs.size) {
                // Check Android version and use appropriate createFromPdu.
                if (isVersionM) {
                    // If Android version M or newer:
                    msgs[i] =
                            SmsMessage.createFromPdu(pdus[i] as ByteArray, format)
                } else {
                    // If Android version L or older:
                    msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                }
                strMessage += "SMS from " + msgs[i].originatingAddress
                strMessage += " :" + msgs[i].messageBody + "\n"
                context?.let { sendNotification(it, strMessage) }
            }
        }
    }

    private fun sendNotification(context: Context, message: String) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        val notifyIntent = Intent(context, MainActivity::class.java)
        notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val notifyPendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val notification = NotificationCompat.Builder(context, "default")
            .setContentTitle(message)
            .setContentText(message)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(notifyPendingIntent)
        notificationManager.notify(1, notification.build())
    }
}