package aakash.com.readsmssample

import aakash.com.readsmssample.injection.component.AppComponent
import aakash.com.readsmssample.injection.component.DaggerAppComponent
import aakash.com.readsmssample.injection.module.MainActivityModule
import android.app.Application

/**
 * Created by stllpt031 on 12/2/19.
 */
class AppApplication : Application() {
    lateinit var mComponent: AppComponent

    companion object {

        private lateinit var instance: AppApplication

        fun getAppContext(): AppApplication {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initDagger()
    }

    private fun initDagger() {
        mComponent = DaggerAppComponent.builder()
            .mainActivityModule(MainActivityModule(getAppContext()))
            .build()
    }
}