package aakash.com.readsmssample.extension

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun Long.getDate(): String {
    val date = Date(this)
    val dateFormat = SimpleDateFormat("EEEE, MMMM dd, yyyy | hh:mm a", Locale.getDefault())
    return dateFormat.format(date)
}

fun Long.checkDifference(): String {
    val date = Date()
    val hours = TimeUnit.MILLISECONDS.toHours(date.time - this)
    return when {
        hours > 24 -> "1 day ago"
        hours > 12 -> "12 hours ago"
        hours > 6 -> "6 hours ago"
        hours > 3 -> "3 hours ago"
        hours > 2 -> "2 hours ago"
        hours > 1 -> "1 hour ago"
        hours in 0..1 -> "Few minutes ago"
        else -> "Few minutes ago"
    }
}

fun Long.getDifferenceInHour() = TimeUnit.MILLISECONDS.toHours(Date().time - this)