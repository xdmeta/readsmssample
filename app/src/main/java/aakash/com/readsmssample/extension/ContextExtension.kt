package aakash.com.readsmssample.extension

import android.app.Activity
import androidx.core.app.ActivityCompat

/**
 * Created by stllpt031 on 12/2/19.
 */
fun Activity.requestRequiredPermission(permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(this, permissions, requestCode)
}