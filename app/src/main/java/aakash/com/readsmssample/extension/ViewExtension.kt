package aakash.com.readsmssample.extension

import android.view.View
import com.google.android.material.snackbar.Snackbar

/**
 * Created by stllpt031 on 12/2/19.
 */
fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun Boolean.isVisible() = if (this) View.VISIBLE else View.GONE

fun View.showSnack(msg: String) {
    Snackbar.make(this, msg, Snackbar.LENGTH_LONG).show()
}